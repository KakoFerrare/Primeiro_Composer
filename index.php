<?php

require_once './vendor/autoload.php';
#use \GUMP;

$dadosForm = ['nome'=>'as', 'email'=>'kako@kako.com'];
$regras = [
    'nome' => 'required|max_len,100|min_len,3',
    'email' => 'required|max_len,150|valid_email'
];

/*$is_valid = GUMP::is_valid($_POST, array(
    'username' => 'required|alpha_numeric',
    'password' => 'required|max_len,100|min_len,6'
));
*/
$is_valid = GUMP::is_valid($dadosForm, $regras);

if($is_valid === true) {
    echo 'Dados cadastrados';
} else {
    print_r($is_valid);
}
